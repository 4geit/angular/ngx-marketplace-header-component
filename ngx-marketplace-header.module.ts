import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxMarketplaceHeaderComponent } from './ngx-marketplace-header.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxSearchBarModule } from '@4geit/ngx-search-bar-component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
    NgxSearchBarModule,
  ],
  declarations: [
    NgxMarketplaceHeaderComponent
  ],
  exports: [
    NgxMarketplaceHeaderComponent
  ]
})
export class NgxMarketplaceHeaderModule { }
