import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { NgxAuthService } from '@4geit/ngx-auth-service';

@Component({
  selector: 'ngx-marketplace-header',
  template: require('pug-loader!./ngx-marketplace-header.component.pug')(),
  styleUrls: ['./ngx-marketplace-header.component.scss']
})
export class NgxMarketplaceHeaderComponent implements OnInit {

  constructor(
    private authService: NgxAuthService,
    private location: Location,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  isPageActive(page: any) {
    if (typeof page === 'string') {
      return page === this.location.path();
    }
    for (let i = 0; i < page.length; i++) {
      if (page[i] === this.location.path()) {
        return true;
      }
    }
    return false;
  }

}
