import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMarketplaceHeaderComponent } from './ngx-marketplace-header.component';

describe('marketplace-header', () => {
  let component: marketplace-header;
  let fixture: ComponentFixture<marketplace-header>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ marketplace-header ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(marketplace-header);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
